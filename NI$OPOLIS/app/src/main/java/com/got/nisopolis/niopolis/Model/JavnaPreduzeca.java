package com.got.nisopolis.niopolis.Model;

/**
 * Created by ProBook on 4/16/2017.
 */

public class JavnaPreduzeca extends Kartica {

    protected String naziv;
    protected long cena;
    protected long dadzbina;
    protected int maxPoseda;
    protected int brPoseda;

    public JavnaPreduzeca() {
        brPoseda = 0;
    }


    public String getNaziv() {
        return naziv;
    }

    public long getCena() {
        return cena;
    }

    public long getDadzbina() {
        return dadzbina;
    }

    public int getMaxPoseda() {
        return maxPoseda;
    }


    public int getBrPoseda() {
        return brPoseda;
    }

    public void setBrPoseda(int brPoseda) {
        this.brPoseda = brPoseda;
    }

}