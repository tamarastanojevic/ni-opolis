package com.got.nisopolis.niopolis.Model;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TAMARA on 17.04.2017..
 */

public class Player {

    private InetAddress IP;
    public int port;
    private String username;
    protected Lista_Kartica svekartice=Lista_Kartica.getInstance();
    protected List<Kartica> mojekartice;
    private int broj_kuca;
    private int broj_hotela;
    private int pozicija;
    protected long novac;
    protected boolean imaHipo;
    protected int brHipo;
    protected long cenaHipo;
    protected Kartica karticaHipo;
    protected boolean zatvor;
    protected int brKrugZatvor;
    protected int figurica;



    public void setNovacKraj(long n)
    {
        this.novac=n;
    }
    public boolean isZatvor() {
        return zatvor;
    }

    public void setZatvor(boolean zatvor) {
        this.zatvor = zatvor;
    }

    public int getFigurica() {
        return figurica;
    }

    public void setFigurica(int figurica) {
        this.figurica = figurica;
    }

    public int getBrKrugZatvor() {
        return brKrugZatvor;
    }

    public void setBrKrugZatvor(int brKrugZatvor) {
        this.brKrugZatvor = brKrugZatvor;
    }



    public Player(InetAddress ipp,  String user)
    {
       IP=ipp;
       username=user;
        broj_hotela=0;
        broj_kuca=0;
        pozicija=0;
        novac=120000;
       // novac=20000;
        mojekartice=new ArrayList<Kartica>();
        imaHipo=false;
        brHipo=0;
        zatvor=false;
        brKrugZatvor=0;
    }


    public int getPozicija() {
        return pozicija;
    }

    public void setPozicija(int poz) {
        this.pozicija = (this.pozicija + poz)%40;
        if(this.pozicija==0)
            this.novac+=20000;
    }
    public void setPozicijaZatvor()
    {
        this.pozicija=10;
    }

    public InetAddress getIP()
    {
        return IP;
    }
    public String getUsername()
    {
        return username;
    }
    int getBroj_kuca()
    {
        return broj_kuca;
    }
    int getBroj_hotela()
    {
        return broj_hotela;
    }
    public void dodajKarticu(int poz)
    {
        mojekartice.add(svekartice.getInstance().lista_kartica.get(poz));
        Kartica k1,k2;
        for(int i=0;i<mojekartice.size()-1;i++)
        {
            k1=mojekartice.get(i);
            for(int j=i+1;j<mojekartice.size();j++)
            {
                if(k1.tag>=mojekartice.get(j).tag)
                {
                    k2=k1;
                    k1=mojekartice.get(j);
                    mojekartice.set(j,k2);
                    mojekartice.set(i,k1);
                }
            }
        }
    }
    public Kartica getKarticaIzListeSvihIme(String ime)
    {
        return svekartice.getKarticaIme(ime);
    }
    public Kartica getKarticuIzListeSvih(int x)
    {
        return svekartice.lista_kartica.get(x);
    }
    public Kartica getKarticuIzListeMojih(int x)
    {
        return mojekartice.get(x);
    }
    public void izbaciKarticu(int poz)
    {
        Kartica kk = svekartice.lista_kartica.get(poz);
        for(int i=0;i<mojekartice.size();i++)
        {
            Kartica k=mojekartice.get(i);
            if(k instanceof Nekretnina && kk instanceof Nekretnina)
            {
                Nekretnina n=(Nekretnina) k;
                Nekretnina n1=(Nekretnina) kk;
                if(n.getNaziv().equals(n1.getNaziv()))
                {
                    setNovac(n.getCena());
                    mojekartice.remove(i);
                }
            }
            else if(k instanceof JavnaPreduzeca && kk instanceof JavnaPreduzeca)
            {
                JavnaPreduzeca jp=(JavnaPreduzeca)k;
                JavnaPreduzeca jp1=(JavnaPreduzeca)kk;
                if(jp.getNaziv().equals(jp1.getNaziv()))
                {
                    setNovac(jp.getCena());
                    mojekartice.remove(i);
                }
            }
        }
    }
    public List<Kartica> getMojekartice()
    {
        return mojekartice;
    }
    public long getNovac() {
        return novac;
    }

    public void setNovac(long novac) {
        this.novac += novac;
    }

    public void postaviHipo(int poz){
        imaHipo=true;
        brHipo=3;
        karticaHipo=svekartice.getKartica(poz);
        if(karticaHipo instanceof Nekretnina){
            Nekretnina n=(Nekretnina)karticaHipo;
            cenaHipo=n.getCena()/2;

        }
        else{
            JavnaPreduzeca jp=(JavnaPreduzeca)karticaHipo;
            cenaHipo=jp.getCena()/2;
        }
        novac+=cenaHipo;
    }

    public boolean proveraHipo(){
        if(imaHipo){
            brHipo--;
            if(brHipo==0) {
                novac -= cenaHipo;
                if (novac <= 0) {
                    return true;
                }
            }
        }
        return false;

    }

    public void izvuciIznenadjenje(int x){
        Iznenadjenje iz=Lista_Iznenadjenja.getInstance().spil_iznenadjenja.get(x);
        String tip=iz.tip;
        String[] poruka = tip.split(" ");
        if(poruka[0].equals("dobitak")){
            int z=Integer.parseInt(poruka[1]);
            this.setNovac(z);
        }
        else if(poruka[0].equals("gubitak")){
            int z=Integer.parseInt(poruka[1]);
            this.setNovac(-z);
        }
        else if(poruka[0].equals("idi")){
            if(poruka[1].equals("do")){
                this.pozicija=Integer.parseInt(poruka[2]);
            }
            else{
                this.pozicija+=Integer.parseInt(poruka[2]);
            }
        }
        else if(poruka[0].equals("porez")){
            int ku=Integer.parseInt(poruka[1]);
            int hot=Integer.parseInt(poruka[2]);
            int kk=this.broj_kuca*ku;
            int hh=this.broj_hotela*hot;
            this.novac=this.novac-(hh+kk);
        }
        else if(poruka[0].equals("zatvor")){
            if(poruka[1].equals("ulaz")){
                this.zatvor=true;
                this.pozicija=10;
                this.brKrugZatvor=3;
            }
            else {
                if(this.zatvor) {
                    this.zatvor = false;
                    this.brKrugZatvor = 0;
                }
                else
                {
                    this.novac+=5000;
                }
            }
        }

    }



    public void izvuciSansu(int x){
        Sansa san=Lista_Sansi.getInstance().spil_sansi.get(x);
        String tip=san.tip;
        String[] poruka = tip.split(" ");
        if(poruka[0].equals("dobitak")){
            int z=Integer.parseInt(poruka[1]);
            this.setNovac(z);
        }
        else if(poruka[0].equals("gubitak")){
            int z=Integer.parseInt(poruka[1]);
            this.setNovac(-z);
        }
        else if(poruka[0].equals("idi")){
            if(poruka[1].equals("do")){
                this.pozicija=Integer.parseInt(poruka[2]);
            }
            else {
                this.pozicija += Integer.parseInt(poruka[2]);
            }
        }
        else if(poruka[0].equals("porez")){
            int ku=Integer.parseInt(poruka[1]);
            int hot=Integer.parseInt(poruka[2]);
            int kk=this.broj_kuca*ku;
            int hh=this.broj_hotela*hot;
            this.novac=this.novac-(hh+kk);
        }
        else if(poruka[0].equals("zatvor")){
            if(poruka[1].equals("ulaz")){
                this.zatvor=true;
                this.pozicija=10;
                this.brKrugZatvor=3;
            }
            else {
                if(this.zatvor) {
                    this.zatvor = false;
                    this.brKrugZatvor = 0;
                }
                else
                {
                    this.novac+=5000;
                }
            }
        }

    }

}
