package com.got.nisopolis.niopolis.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by ProBook on 4/16/2017.
 *
 *
 *
 * Treba da se se sve ovo izvede iz polja!!!!!
 */

public class Lista_Sansi extends Polje{

    private static volatile Lista_Sansi instance=null;
    protected List<Sansa> spil_sansi;

    protected Lista_Sansi()
    {
        spil_sansi=new ArrayList<Sansa>();

        spil_sansi.add(new Sansa(){{text_sanse="Idite ravno u zatvor, a da ne predjete preko START-a."; izvucena =false; tip="zatvor ulaz";}});
        spil_sansi.add(new Sansa(){{text_sanse="Nasledjujete od dalekog rodjaka 10000!"; izvucena =false; tip="dobitak 10000";}});
        spil_sansi.add(new Sansa(){{text_sanse="Platite racun lekaru 5000!"; izvucena =false; tip="gubitak 5000";}});
        spil_sansi.add(new Sansa(){{text_sanse="Platite kamatu u iznosu od 3000!"; izvucena =false; tip="gubitak 3000";}});
        spil_sansi.add(new Sansa(){{text_sanse="Ako ste u zatvoru mozete besplatno izaci."; izvucena =false; tip="zatvor izlaz";}}); //FLEG SPECIJALNA!!!
        //prodaja te kartice - eventualno izbaciti
        //flegovi placanje uzimanje novca, idi na nesto - br polja/ id kartice
        spil_sansi.add(new Sansa(){{text_sanse="Dobitnik ste utesne nagrade lutrije u Becu. Podignite 10000!"; izvucena =false; tip="dobitak 10000";}});
        spil_sansi.add(new Sansa(){{text_sanse="Platite kaznu od 1000!"; izvucena =false; tip="gubitak 1000";}});
        spil_sansi.add(new Sansa(){{text_sanse="Izgubili ste parnicu, platite 10000!"; izvucena =false; tip="gubitak 10000";}});
        spil_sansi.add(new Sansa(){{text_sanse="Prodali ste akcije! Podignite 5000!"; izvucena =false; tip="dobitak 5000";}});
        spil_sansi.add(new Sansa(){{text_sanse="Dospevaju dividende na vase akcije. Podignite 2500!"; izvucena =false; tip="dobitak 2500";}});
        spil_sansi.add(new Sansa(){{text_sanse="Plati porez na prihod 2000!"; izvucena =false; tip="gubitak 2000";}});
        spil_sansi.add(new Sansa(){{text_sanse="Poverilac ste banke u iznosu od 20000. Podignite ih!"; izvucena =false; tip="dobitak 20000";}});
        spil_sansi.add(new Sansa(){{text_sanse="Idite do START-a!"; izvucena =false; tip="idi do 0";}});
        spil_sansi.add(new Sansa(){{text_sanse="Vratite se do TC Kalce!"; izvucena =false; tip="idi do 1";}});
        spil_sansi.add(new Sansa(){{text_sanse="Idite do Caira!"; izvucena =false; tip="idi do 18";}});
        spil_sansi.add(new Sansa(){{text_sanse="Dobitnik ste druge nagrade na takmicenju lepote. Podignite 1000!"; izvucena =false; tip="dobitak 1000";}});
    }

    public static Lista_Sansi getInstance()
    {
        if(instance==null)
        {
            synchronized (Lista_Sansi.class)
            {
                if(instance==null)
                {
                    instance=new Lista_Sansi();
                }
            }
        }
        return instance;
    }



    public Sansa Izvuci_Sansu()
    {
        Sansa zaIzvlacenje=new Sansa(){{text_sanse=" "; izvucena =false;}};
        Random num=new Random();
        int n=0;

        while(!zaIzvlacenje.izvucena)
        {
            n=num.nextInt(16);
        //    n=12;
            zaIzvlacenje=spil_sansi.get(n);
            zaIzvlacenje.izvucena =true;
        }

        spil_sansi.add(n,zaIzvlacenje);

        return  zaIzvlacenje;
    }
////////////////////////////////////////////////////////////////////
// //////////// SLUCAJ KAD SU SVE IZVUCENE /////////////////////////

    public Sansa GetSansa(int x)
    {
        return spil_sansi.get(x);
    }

}
