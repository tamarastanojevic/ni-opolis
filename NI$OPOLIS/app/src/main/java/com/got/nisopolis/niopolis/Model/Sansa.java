package com.got.nisopolis.niopolis.Model;

/**
 * Created by ProBook on 4/16/2017.
 */

public class Sansa extends Kartica {

    protected String tip;
    protected String text_sanse;
    protected boolean izvucena;



    public boolean getIzvucena() {
        return izvucena;
    }

    public void setIzvucena(boolean x) {
        this.izvucena = x;
    }

    public String getText_sanse() {
        return text_sanse;
    }

    public void setText_sanse(String x) {
        this.text_sanse=x;
    }

}
