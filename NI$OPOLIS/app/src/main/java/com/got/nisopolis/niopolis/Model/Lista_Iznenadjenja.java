package com.got.nisopolis.niopolis.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by ProBook on 4/16/2017.
 */

public class Lista_Iznenadjenja extends Polje{

    private static volatile Lista_Iznenadjenja instance=null;
    protected List<Iznenadjenje> spil_iznenadjenja;

    protected Lista_Iznenadjenja()
    {
        spil_iznenadjenja=new ArrayList<Iznenadjenje>();

        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Banka vam isplacuje kamate na vas tekuci racun: podignite 5000!"; izvuceno =false; tip="dobitak 5000";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Dajte 2000 u dobrotvorne svrhe."; izvuceno =false; tip="gubitak 2000";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Dobili ste na lutriji, podignite 20000!"; izvuceno =false; tip="dobitak 20000";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Idite do Dusanove."; izvuceno =false; tip="idi do 13";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Idite napred do START-a!"; izvuceno =false; tip="idi do 0";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Dobili ste neocekivanu nagradu! Podignite 10000!"; izvuceno =false; tip="dobitak 10000";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Dospevaju kamate na Vasu rentu. Podignite 15000!"; izvuceno =false; tip="dobitak 15000";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Idite do Doma Vojske."; izvuceno =false; tip="idi do 24";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Morate platiti doprinose za puteve, 4000 za svaku kucu i 10000 za svaki hotel koji posedujete!"; izvuceno =false; tip="porez 4000 10000";}});
        //count broj kuca ukupan i broj hotela ukupan kod Player-a da bi se sracunalo

        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Vencanje u porodici! Nepredvidivi troskovi 15000."; izvuceno =false; tip="gubitak 15000";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Kazna od 1500 zbog voznje bez vozacke."; izvuceno =false; tip="gubitak 1500";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Idite do Naselja Beverli Hils."; izvuceno =false; tip="idi do 37";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Idite 3 koraka unazad, uz mnogo srece!"; izvuceno =false; tip="idi korak -3";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Ako ste u zatvoru mozete besplatno izaci."; izvuceno =false; tip="zatvor izlaz";}});
        //eventualno izbacimo prodaju ove kartice i olaksamo sebi zivot :D
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Idite ravno u ZATVOR a da ne predjete preko START-a!"; izvuceno =false; tip="zatvor ulaz";}});
        spil_iznenadjenja.add(new Iznenadjenje(){{tag=0; textIznenadjenja="Morate popraviti sve Vase nekretnine. Platite 2500 za svaku kucu i 10000 za svaki hotel!"; izvuceno =false; tip="porez 2500 10000";}});
        //isto za count br kuca i hotela
    }


    public  static Lista_Iznenadjenja getInstance()
    {
        if(instance==null)
        {
            synchronized (Lista_Iznenadjenja.class) {
                if(instance==null){
                    instance=new Lista_Iznenadjenja();
                }
            }
        }
        return instance;
    }

    public Iznenadjenje Izvuci_Iznenadjenje()
    {
        Iznenadjenje zaIzvlacenje=new Iznenadjenje(){{textIznenadjenja=" "; izvuceno =false;}};
        Random num=new Random();
        int n=0;

        while(!zaIzvlacenje.izvuceno)
        {
            n=num.nextInt(16);
            zaIzvlacenje=spil_iznenadjenja.get(n);
            zaIzvlacenje.izvuceno=true;
        }

        spil_iznenadjenja.add(n,zaIzvlacenje);

        return  zaIzvlacenje;
    }

    public Iznenadjenje GetIznenadjenje(int x)
    {
        return spil_iznenadjenja.get(x);
    }
}
