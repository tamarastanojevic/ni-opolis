package com.got.nisopolis.niopolis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Igrac extends AppCompatActivity {

    public Button dalje;
    public String name;
    public EditText ed1, edime;
    public String ssss;

    public void convert()
    {
        ed1=(EditText)findViewById(R.id.etIP1);
        edime=(EditText) findViewById(R.id.ime);

        dalje=(Button)findViewById(R.id.btnDalje);
        dalje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ssss=ed1.getText().toString();
                String name=edime.getText().toString();

                Intent intent = new Intent(Igrac.this, Pravi_Igrac.class);
                intent.putExtra("ip",ssss);
                intent.putExtra("username",name);
                startActivity(intent);
            }
        });

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_igrac);

        convert();

    }
}
