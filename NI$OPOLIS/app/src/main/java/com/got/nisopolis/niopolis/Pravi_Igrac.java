package com.got.nisopolis.niopolis;

import android.app.Activity;
import android.app.AlertDialog;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.got.nisopolis.niopolis.Model.JavnaPreduzeca;
import com.got.nisopolis.niopolis.Model.Kartica;
import com.got.nisopolis.niopolis.Model.Lista_Iznenadjenja;
import com.got.nisopolis.niopolis.Model.Lista_Kartica;
import com.got.nisopolis.niopolis.Model.Lista_Sansi;
import com.got.nisopolis.niopolis.Model.Nekretnina;
import com.got.nisopolis.niopolis.Model.Player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

public class Pravi_Igrac extends Activity {

    private static final int SECOND_ACTIVITY_RESULT_CODE = 0;
    TextView textResponse;
    Button buttonConnect, buttonBaci, buttonOdgovor, buttonKupi, buttonOdbij, buttonPregledKartica, buttonZvonce;
    public String ip, username, st;
    Player ja;
    Queue<String> bufferPoruka=new ArrayBlockingQueue<String>(20);
    EditText welcomeMsg;
    MediaPlayer mp;


    public void Odgovor()
    {
        buttonOdgovor=(Button) findViewById(R.id.btnOdgovor);
        buttonZvonce=(Button) findViewById(R.id.btnOdgovor1);
        buttonConnect.setVisibility(View.VISIBLE);
        buttonOdgovor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (bufferPoruka.peek() == null)
                        throw new Exception("Red je prazan!");
                    else {
                        String por = bufferPoruka.peek();
                        final String[] poruka = por.split(" ");

                        if (poruka[0].equals("zauzeto")) {

                            if(poruka[1].equals("ja"))
                            {
                                LayoutInflater lej= LayoutInflater.from(buttonOdgovor.getContext());
                                final View del= lej.inflate(R.layout.activity_zauzeto_ja,null);
                                final AlertDialog al= new AlertDialog.Builder(buttonOdgovor.getContext()).create();

                                al.setView(del);

                                Button dug=(Button)del.findViewById(R.id.dismiss8);
                                dug.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        al.dismiss();
                                    }
                                });

                                al.show();

                            }
                            else
                            {
                                LayoutInflater lej= LayoutInflater.from(buttonOdgovor.getContext());
                                final View del= lej.inflate(R.layout.activity_zauzeto,null);
                                final AlertDialog al= new AlertDialog.Builder(buttonOdgovor.getContext()).create();

                                al.setView(del);

                                Button dug=(Button)del.findViewById(R.id.dismiss9);
                                dug.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        al.dismiss();
                                    }
                                });

                                al.show();

                                ja.setNovac(-Integer.parseInt(poruka[1]));
                            }
                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.INVISIBLE);

                            por = bufferPoruka.poll();
                            OsveziNovac();
                        } else if (poruka[0].equals("nekretnina")) {
                            Kartica k = ja.getKarticuIzListeSvih(Integer.parseInt(poruka[1]));
                            if (k instanceof Nekretnina) {
                                Nekretnina n = (Nekretnina) k;
                                buttonConnect.setVisibility(View.VISIBLE);
                                buttonOdgovor.setVisibility(View.INVISIBLE);
                                buttonZvonce.setVisibility(View.VISIBLE);

                                int pozi;

                                String x = n.getNaziv();
                                pozi = Lista_Kartica.getInstance().getPozicijaKartice(x);

                                String pom="k"+Integer.toString(pozi);
                                final String ime=pom+"_poz";
                                final int poziX=pozi;

                                LayoutInflater lej= LayoutInflater.from(buttonOdgovor.getContext());
                                final View del= lej.inflate(R.layout.activity_kupovina_kartice,null);
                                final AlertDialog al= new AlertDialog.Builder(buttonOdgovor.getContext()).create();

                                al.setView(del);

                                ImageView im=(ImageView)del.findViewById(R.id.slikaKupovina);
                                im.setImageResource(getResources().getIdentifier(ime.toString(),"drawable",getPackageName()));

                                Button dugKupi=(Button)del.findViewById(R.id.btnKupi);
                                dugKupi.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Kupi();
                                        al.dismiss();
                                    }
                                });

                                Button dugOdbij=(Button)del.findViewById(R.id.btnOdbij);
                                dugOdbij.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Odbij();
                                        al.dismiss();
                                    }
                                });

                                al.show();

                            }
                            OsveziNovac();
                        } else if (poruka[0].equals("javno_preduzece")) {
                            Kartica k = ja.getKarticuIzListeSvih(Integer.parseInt(poruka[1]));
                            if (k instanceof JavnaPreduzeca) {
                                JavnaPreduzeca n = (JavnaPreduzeca) k;
                                buttonConnect.setVisibility(View.VISIBLE);
                                buttonOdgovor.setVisibility(View.INVISIBLE);
                                buttonZvonce.setVisibility(View.VISIBLE);

                                int pozi;

                                String x = n.getNaziv();
                                pozi = Lista_Kartica.getInstance().getPozicijaKartice(x);

                                String pom="k"+Integer.toString(pozi);
                                final String ime=pom+"_poz";
                                final int poziX=pozi;

                                LayoutInflater lej= LayoutInflater.from(buttonOdgovor.getContext());
                                final View del= lej.inflate(R.layout.activity_kupovina_kartice,null);
                                final AlertDialog al= new AlertDialog.Builder(buttonOdgovor.getContext()).create();

                                al.setView(del);

                                ImageView im=(ImageView)del.findViewById(R.id.slikaKupovina);
                                im.setImageResource(getResources().getIdentifier(ime.toString(),"drawable",getPackageName()));

                                Button dugKupi=(Button)del.findViewById(R.id.btnKupi);
                                dugKupi.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Kupi();
                                        al.dismiss();
                                    }
                                });

                                Button dugOdbij=(Button)del.findViewById(R.id.btnOdbij);
                                dugOdbij.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Odbij();
                                        al.dismiss();
                                    }
                                });

                                al.show();
                            }

                            OsveziNovac();
                        } else if (poruka[0].equals("odbij") && poruka[1].equals("ok")) {
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.INVISIBLE);

                            por = bufferPoruka.poll();
                            OsveziNovac();
                        } else if (poruka[0].equals("kupljeno")) {
                            int poz = Integer.parseInt(poruka[1]);
                            ja.dodajKarticu(poz);
                            Kartica k = ja.getKarticuIzListeSvih(poz);
                            if (k instanceof Nekretnina) {
                                Nekretnina n = (Nekretnina) k;
                                ja.setNovac(-n.getCena());
                            } else if (k instanceof JavnaPreduzeca) {
                                JavnaPreduzeca jp = (JavnaPreduzeca) k;
                                ja.setNovac(-jp.getCena());
                            }
                            por = bufferPoruka.poll();
                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            PrikazKartica();

                            OsveziNovac();
                        }
                        else if (poruka[0].equals("porez"))
                        {
                            ja.setNovac(-(Integer.parseInt(poruka[1])));
                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.INVISIBLE);

                            LayoutInflater lej= LayoutInflater.from(buttonOdgovor.getContext());
                            final View del= lej.inflate(R.layout.activity_porez,null);
                            final AlertDialog al= new AlertDialog.Builder(buttonOdgovor.getContext()).create();

                            al.setView(del);

                            Button dug=(Button)del.findViewById(R.id.dismiss7);
                            dug.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    al.dismiss();
                                }
                            });

                            al.show();

                            por = bufferPoruka.poll();
                            OsveziNovac();
                        }
                        else if(poruka[0].equals("prodato"))
                        {
                            int pozicija=Integer.parseInt(poruka[1]);
                            ja.izbaciKarticu(pozicija);

                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.INVISIBLE);

                            PrikazKartica();
                            por= bufferPoruka.poll();
                            OsveziNovac();
                        }
                        else if(poruka[0].equals("hipoteka") && poruka[1].equals("ok"))
                        {
                            int pozicija=Integer.parseInt(poruka[2]);
                            ja.postaviHipo(pozicija);

                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.INVISIBLE);

                            por=bufferPoruka.poll();
                            OsveziNovac();
                        }
                        else if(poruka[0].equals("sansa")){
                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.INVISIBLE);

                            ja.izvuciSansu(Integer.parseInt(poruka[1]));

                            LayoutInflater lej= LayoutInflater.from(buttonOdgovor.getContext());
                            final View del= lej.inflate(R.layout.activity_sansa_iznenadjenje,null);
                            final AlertDialog al= new AlertDialog.Builder(buttonOdgovor.getContext()).create();

                            al.setView(del);

                            TextView tv = (TextView) del.findViewById(R.id.textSanIzn);
                            tv.setText((Lista_Sansi.getInstance().GetSansa(Integer.parseInt(poruka[1]))).getText_sanse());

                            Button dug=(Button)del.findViewById(R.id.dismiss3);
                            dug.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    al.dismiss();
                                }
                            });

                            al.show();

                            por = bufferPoruka.poll();
                            OsveziNovac();
                        }
                        else if(poruka[0].equals("iznenadjenje")){
                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.INVISIBLE);

                            ja.izvuciIznenadjenje(Integer.parseInt(poruka[1]));

                            LayoutInflater lej= LayoutInflater.from(buttonOdgovor.getContext());
                            final View del= lej.inflate(R.layout.activity_sansa_iznenadjenje,null);
                            final AlertDialog al= new AlertDialog.Builder(buttonOdgovor.getContext()).create();

                            al.setView(del);

                            TextView tv = (TextView) del.findViewById(R.id.textSanIzn);
                            tv.setText((Lista_Iznenadjenja.getInstance().GetIznenadjenje(Integer.parseInt(poruka[1]))).getTextIznenadjenja());

                            Button dug=(Button)del.findViewById(R.id.dismiss3);
                            dug.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    al.dismiss();
                                }
                            });

                            al.show();

                            por = bufferPoruka.poll();
                            OsveziNovac();
                        }
                        else if(poruka[0].equals("parking")){
                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.INVISIBLE);
                            ja.setNovac(Integer.parseInt(poruka[1]));
                            por = bufferPoruka.poll();

                            LayoutInflater lej= LayoutInflater.from(buttonOdgovor.getContext());
                            final View del= lej.inflate(R.layout.activity_parking,null);
                            final AlertDialog al= new AlertDialog.Builder(buttonOdgovor.getContext()).create();

                            al.setView(del);

                            Button dug=(Button)del.findViewById(R.id.dismiss5);
                            dug.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    al.dismiss();
                                }
                            });

                            al.show();
                            OsveziNovac();
                        }
                        else if(poruka[0].equals("zatvor")){
                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.INVISIBLE);

                            if(poruka.length>1)
                            {
                                ja.setBrKrugZatvor(Integer.parseInt(poruka[2]));
                            }
                            else {
                                LayoutInflater lej = LayoutInflater.from(buttonOdgovor.getContext());
                                final View del = lej.inflate(R.layout.activity_zatvor, null);
                                final AlertDialog al = new AlertDialog.Builder(buttonOdgovor.getContext()).create();

                                al.setView(del);

                                Button dug = (Button) del.findViewById(R.id.dismiss2);
                                dug.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        al.dismiss();
                                    }
                                });

                                al.show();
                            }

                            por = bufferPoruka.poll();
                            OsveziNovac();
                        }
                        else if(poruka[0].equals("prolaz")){
                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.INVISIBLE);
                            por = bufferPoruka.poll();

                            LayoutInflater lej= LayoutInflater.from(buttonOdgovor.getContext());
                            final View del= lej.inflate(R.layout.activity_prolaz,null);
                            final AlertDialog al= new AlertDialog.Builder(buttonOdgovor.getContext()).create();

                            al.setView(del);

                            Button dug=(Button)del.findViewById(R.id.dismiss6);
                            dug.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    al.dismiss();
                                }
                            });

                            al.show();

                            OsveziNovac();
                        }
                        else if(poruka[0].equals("Zavrsen") && poruka[1].equals("potez"))
                        {
                            por = bufferPoruka.poll();
                            buttonConnect.setVisibility(View.INVISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                            buttonBaci.setVisibility(View.VISIBLE);

                            ja.setNovacKraj(Integer.parseInt(poruka[2]));
                            OsveziNovac();
                        }
                        else if(poruka[0].equals("start"))
                        {
                            ja.setNovac(Integer.parseInt(poruka[1]));
                            buttonBaci.setVisibility(View.INVISIBLE);
                            buttonConnect.setVisibility(View.VISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);

                            por=bufferPoruka.poll();
                            OsveziNovac();
                        }
                        else if(poruka[0].equals("Nije"))
                        {
                            por = bufferPoruka.poll();
                            buttonBaci.setVisibility(View.VISIBLE);
                            buttonConnect.setVisibility(View.INVISIBLE);
                            buttonOdgovor.setVisibility(View.INVISIBLE);
                            buttonZvonce.setVisibility(View.VISIBLE);
                        }
                        else if(poruka[0].equals("Kraj") && poruka[1].equals("igre"))
                        {
                            mp =MediaPlayer.create(buttonOdgovor.getContext(),R.raw.kraj);
                            mp.start();

                            SystemClock.sleep(10000);

                            finish();
                            System.exit(0);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void OsveziNovac()
    {
        TextView noTextView=(TextView) findViewById(R.id.novac);
        int n=(int)(ja.getNovac());
        noTextView.setText(" "+n);
    }

    public void Kupi()
    {
        buttonConnect.setVisibility(View.INVISIBLE);
        String por = bufferPoruka.poll();
        String[] poruka = por.split(" ");
        String p = username + " kupi ok " + poruka[1];

        Kartica k = ja.getKarticuIzListeSvih(Integer.parseInt(poruka[1]));
        if(k instanceof Nekretnina) {
            Nekretnina n = (Nekretnina) k;
            if (ja.getNovac() < n.getCena()) {
                Odbij();
            }
            else {
                MyClientTask myClientTask = new MyClientTask(ip, 5151, p);
                myClientTask.execute();
            }
        }
        else
        {
            JavnaPreduzeca jp=(JavnaPreduzeca)k;
            if(ja.getNovac()<jp.getCena()) {
                Odbij();
            }
            else {
                MyClientTask myClientTask = new MyClientTask(ip, 5151, p);
                myClientTask.execute();
            }
        }


    }
    public void Odbij()
    {
        buttonConnect.setVisibility(View.INVISIBLE);
        String a=bufferPoruka.poll();
        String p=username+" odbij";

        MyClientTask myClientTask = new MyClientTask(ip, 5151, p);
        myClientTask.execute();
    }
    public void Kraj()
    {
        buttonBaci.setVisibility(View.VISIBLE);
        buttonConnect=(Button) findViewById(R.id.btnZavrsiPotez);
        buttonConnect.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View arg0)
            {

                String tMsg=username +" kraj";
                buttonConnect.setVisibility(View.INVISIBLE);

                MyClientTask myClientTask = new MyClientTask(ip, 5151, tMsg);
                myClientTask.execute();

            }
        });

    }

    public void Bacaj()
    {

        final Random rad=new Random();

        buttonBaci=(Button)findViewById(R.id.btnBaci);

        buttonBaci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer x;
                x=rad.nextInt(11)+2;

                mp =MediaPlayer.create(buttonBaci.getContext(), R.raw.kockica);
                mp.start();

             //   x=30;

                String p=username+" bacaj "+x.toString();

                buttonBaci.setVisibility(View.INVISIBLE);

                MyClientTask myClientTask = new MyClientTask(ip, 5151, p);
                myClientTask.execute();

            }
        });

    }

    public void PrikazKartica() {
       LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearH);
        linearLayout.removeAllViews();


// Add Buttons
        for (int i = 0; i < ja.getMojekartice().size(); i++) {
            final ImageButton button = new ImageButton(this);
            button.setId(i);

            Kartica k=ja.getMojekartice().get(i);
            Nekretnina n;
            JavnaPreduzeca jp;

            int pozi;

            if(k instanceof Nekretnina) {
                n = (Nekretnina) k;
                String x = n.getNaziv();
                pozi = Lista_Kartica.getInstance().getPozicijaKartice(x);
            }
            else {
                jp = (JavnaPreduzeca) k;
                String x = jp.getNaziv();
                pozi = Lista_Kartica.getInstance().getPozicijaKartice(x);
            }

            String pom="k"+Integer.toString(pozi);
            final String ime=pom+"_poz";
            final int poziX=pozi;

            button.setImageResource(getResources().getIdentifier(pom,"drawable",getPackageName()));

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mp =MediaPlayer.create(button.getContext(), R.raw.kartica);
                    mp.start();

                    LayoutInflater lej= LayoutInflater.from(button.getContext());
                    final View del= lej.inflate(R.layout.popup,null);
                    final AlertDialog al= new AlertDialog.Builder(button.getContext()).create();

                    al.setView(del);
                    ImageView im=(ImageView)del.findViewById(R.id.slikaPopUp);
                    im.setImageResource(getResources().getIdentifier(ime.toString(),"drawable",getPackageName()));

                    Button dug=(Button)del.findViewById(R.id.dismiss);
                    dug.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            al.dismiss();
                        }
                    });

                    Button dugProdaj=(Button)del.findViewById(R.id.prodajK);
                    dugProdaj.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ProdajKarticu(poziX);
                            al.dismiss();
                        }
                    });

                    Button dugHipoteka=(Button)del.findViewById(R.id.hipoteka);
                    dugHipoteka.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Hipoteka(poziX);
                            al.dismiss();
                        }
                    });


                    al.show();

                }
            });

            linearLayout.addView(button);
        }
    }

    public void ProdajKarticu(int x)
    {

        String p=username+" prodaj karticu "+x;

        MyClientTask myClientTask = new MyClientTask(ip, 5151, p);
        myClientTask.execute();
    }

    public void Hipoteka(int x)
    {

        String p=username+" hipoteka "+x;

        MyClientTask myClientTask = new MyClientTask(ip, 5151, p);
        myClientTask.execute();
    }


    public String Desifruj(String x){

        String stvarniIP="";

        for(int i=0; i<x.length(); i++){

            switch(x.charAt(i)){
                case 'w':
                    stvarniIP+="1";
                    break;
                case 'z':
                    stvarniIP+="2";
                    break;
                case 'l':
                    stvarniIP+="3";
                    break;
                case 'x':
                    stvarniIP+="4";
                    break;
                case 'f':
                    stvarniIP+="5";
                    break;
                case 'm':
                    stvarniIP+="6";
                    break;
                case 'c':
                    stvarniIP+="7";
                    break;
                case 'y':
                    stvarniIP+="8";
                    break;
                case 'j':
                    stvarniIP+="9";
                    break;
                case 'b':
                    stvarniIP+="0";
                    break;
                case 'a':
                    stvarniIP+=".";
                    break;
            }

        }
        return stvarniIP;
    }

    public class MyClientTask extends AsyncTask<Void, Void, Void>
    {
        String dstAddress;
        int dstPort;
        String response="";
        String msgToServer;

        MyClientTask(String addr, int port, String msgTo)
        {
            dstAddress=addr;
            dstPort=port;
            msgToServer=msgTo;
        }

        @Override
        protected Void doInBackground(Void...arg0)
        {
            Socket socket=null;
            DataOutputStream dataOutputStream = null;
            DataInputStream dataInputStream = null;

            try {
                socket = new Socket(dstAddress, dstPort);
                dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataInputStream = new DataInputStream(socket.getInputStream());

                if(msgToServer != null)
                {
                    dataOutputStream.writeUTF(msgToServer);
                }

                response = dataInputStream.readUTF();

                if(!response.equals("")) {
                    bufferPoruka.add(response);

                    Pravi_Igrac.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            buttonZvonce.setVisibility(View.INVISIBLE);
                            buttonOdgovor.setVisibility(View.VISIBLE);

                            mp =MediaPlayer.create(Pravi_Igrac.this, R.raw.zvono);
                            mp.start();
                        }
                    });
                }

            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
                response = "UnknownHostException: " + e.toString();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                response = "IOException: " + e.toString();
            }
            finally
            {
                if (socket != null)
                {
                    try
                    {
                        socket.close();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }

                if (dataOutputStream != null)
                {
                    try
                    {
                        dataOutputStream.close();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }

                if (dataInputStream != null)
                {
                    try
                    {
                        dataInputStream.close();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            //textResponse.setText(response);

            super.onPostExecute(result);
        }



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pravi__igrac);


        st=getIntent().getExtras().getString("ip");
        username=getIntent().getExtras().getString("username");

        ip=Desifruj(st);

        MyClientTask myClientTask = new MyClientTask(ip, 5151, username);
        myClientTask.execute();

        ja=new Player(null,username);



        Bacaj();
        Kraj();
        Odgovor();
      //  Kupi();
      //  Odbij();
        OsveziNovac();
    }

}